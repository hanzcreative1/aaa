<?php defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;
use chriskacerguis\RestServer\Format;
use \Firebase\JWT\JWT;

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding, x-token");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

class MY_Controller extends RestController
{
	/**
	 * Data token
	 * 
	 * @var array
	 * @access protected
	 */
	protected $data_token = [];

	/**
	 * Constructor
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->load->model('global/M_login', 'm_login');

		$token = $this->input->get_request_header('x-token');
		$check_token = $this->m_login->check_token($token);

		if ($check_token['status'] == 401) {
			$res = [
				'status' => 401,
				'message' => $check_token['message'],
				'data' => []
			];

			$this->response($res, $res['status']);
		} else {
			$this->data_token = $check_token['data'];
		}

		if (empty($this->data_token['access'])) {
			$res = [
				'status' => 401,
				'message' => 'Tidak memiliki akses',
				'data' => []
			];

			$this->response($res, $res['status']);
		}

		if ($this->data_token['access'] !== 'user') {
			$res = [
				'status' => 401,
				'message' => 'Tidak memiliki akses',
				'data' => []
			];

			$this->response($res, $res['status']);
		}
	}

	/**
	 * Query_check
	 * Untuk melihat apakah query berhasil dieksekusi
	 * 
	 */
	protected function query_check($data = [])
	{
		if ($data['status'] != '200') {
			$this->db->trans_rollback();
			$this->response($data, $data['status']);
			die(); // Hentikan semua proses
		} else {
			return true;
		}
	}
}
