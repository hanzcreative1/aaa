<?php
defined('BASEPATH') or exit('No direct script allowed');

class M_cafe_jurnal_transaksi extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->_table = 'cafe_jurnal_transaksi';
    }

    // Gget serverside
    public function get($params)
    {
        $params['order_column'] = [
            'b.id',
            'a.tgl_transaksi',
            'a.nama_transaksi',
            'a.jenis_transaksi',
            'a.jumlah_transaksi',
            'b.total_transaksi',
            'b.bayar',
            'b.kembali',
            'b.total_transaksi',
            'b.nama',
            'b.email',
            'b.telp',
            'b.created_date',
            'b.created_by',
        ];
        $params['column_search'] = [
            'b.id',
            'a.tgl_transaksi',
            'a.nama_transaksi',
            'a.jenis_transaksi',
            'a.jumlah_transaksi',
            'b.total_transaksi',
            'b.bayar',
            'b.kembali',
            'b.total_transaksi',
            'b.nama',
            'b.email',
            'b.telp',
            'b.created_date',
            'b.created_by',
        ];

        $params['order'] = array('id' => 'asc');

        $this->db->select('
            a.nama_transaksi,
            a.tgl_transaksi,
            a.jenis_transaksi,
            a.jumlah_transaksi,
            b.id,
            b.kode_transaksi,
            b.total_transaksi,
            b.nama,
            b.email,
            b.telp,
            b.created_date,
            b.created_by,
            b.bayar,
            b.kembali
        ');
        $this->db->from('cafe_jurnal_transaksi a');
        $this->db->join('cafe_jurnal b', 'a.id_cafe_jurnal = b.id', 'left');


        if (isset($params['tgl_start']) && $params['tgl_start'] != null && isset($params['tgl_end']) && $params['tgl_end'] != null) {
            $this->db->where('DATE(b.created_date) >=', $params['tgl_start']);
            $this->db->where('DATE(b.created_date) <=', $params['tgl_end']);
        }

        if (isset($params['cafe_transaksi']) && $params['cafe_transaksi'] != null) {
            $this->db->where('b.jenis_transaksi', $params['cafe_transaksi']);
        }

        if (isset($params['jurnal_cafe_transaksi']) && $params['jurnal_cafe_transaksi'] != null) {
            $this->db->where('a.jenis_transaksi', $params['jurnal_cafe_transaksi']);
        }

        if (isset($params['id']) && $params['id'] != null) {
            $this->db->where('a.id', $params['id']);
        }

        if (!isset($params['offset'])) {
            $params['offset'] = '0';
        }

        if (isset($params['limit']) && $params['limit'] > 0) {
            $this->db->limit($params['limit'], $params['offset']);
        }

        $this->_query_datatable($params);

        $result = $this->db->get()->result_array();

        return [
            'result' => $result,
            'record_total' => $this->_getTotal(),
            'record_filter' => $this->_getFiltered($params),
        ];
    }

    private function _query_datatable($params)
    {
        $i = 0;
        if (isset($params['search']) && $params['search'] != '') {
            $this->db->group_start();
            foreach ($params['column_search'] as $key => $items) {
                if ($i == 0) {
                    $this->db->like($items, $params['search']);
                } else {
                    $this->db->or_like($items, $params['search']);
                }
                $i++;
            }
            $this->db->group_end();
        }

        if (isset($params['sort_column'])) {
            if ($params['sort_column'] == 0) {
                $column_ui = 0;
            } else {
                $column_ui = $params['sort_column'];
            }
        }

        if (isset($params['sort_column'])) {
            $this->db->order_by($params['order_column'][$column_ui], $params['sort_type']);
        } else {
            $this->db->order_by(key($params['order']), $params['order'][key($params['order'])]);
        }
    }

    private function _getTotal()
    {
        $this->db->select('
            a.id
        ');

        $this->db->from('cafe_jurnal_transaksi a');
        $this->db->join('cafe_jurnal b', 'a.id_cafe_jurnal = b.id', 'left');

        if (isset($params['tgl_start']) && $params['tgl_start'] != null && isset($params['tgl_end']) && $params['tgl_end'] != null) {
            $this->db->where('DATE(b.created_date) >=', $params['tgl_start']);
            $this->db->where('DATE(b.created_date) <=', $params['tgl_end']);
        }

        if (isset($params['cafe_transaksi']) && $params['cafe_transaksi'] != null) {
            $this->db->where('b.jenis_transaksi', $params['cafe_transaksi']);
        }

        if (isset($params['jurnal_cafe_transaksi']) && $params['jurnal_cafe_transaksi'] != null) {
            $this->db->where('a.jenis_transaksi', $params['jurnal_cafe_transaksi']);
        }

        if (isset($params['id']) && $params['id'] != null) {
            $this->db->where('a.id', $params['id']);
        }

        return $this->db->get()->num_rows();
    }

    private function _getFiltered($params)
    {
        $params['order_column'] = [
            'b.id',
            'a.tgl_transaksi',
            'a.nama_transaksi',
            'a.jenis_transaksi',
            'a.jumlah_transaksi',
            'b.total_transaksi',
            'b.bayar',
            'b.kembali',
            'b.total_transaksi',
            'b.nama',
            'b.email',
            'b.telp',
            'b.created_date',
            'b.created_by',
        ];
        $params['column_search'] = [
            'b.id',
            'a.tgl_transaksi',
            'a.nama_transaksi',
            'a.jenis_transaksi',
            'a.jumlah_transaksi',
            'b.total_transaksi',
            'b.bayar',
            'b.kembali',
            'b.total_transaksi',
            'b.nama',
            'b.email',
            'b.telp',
            'b.created_date',
            'b.created_by',
        ];

        $params['order'] = array('id' => 'asc');

        $this->db->select('
            a.id
        ');

        $this->db->from('cafe_jurnal_transaksi a');
        $this->db->join('cafe_jurnal b', 'a.id_cafe_jurnal = b.id', 'left');


        if (isset($params['tgl_start']) && $params['tgl_start'] != null && isset($params['tgl_end']) && $params['tgl_end'] != null) {
            $this->db->where('DATE(b.created_date) >=', $params['tgl_start']);
            $this->db->where('DATE(b.created_date) <=', $params['tgl_end']);
        }

        if (isset($params['cafe_transaksi']) && $params['cafe_transaksi'] != null) {
            $this->db->where('b.jenis_transaksi', $params['cafe_transaksi']);
        }

        if (isset($params['jurnal_cafe_transaksi']) && $params['jurnal_cafe_transaksi'] != null) {
            $this->db->where('a.jenis_transaksi', $params['jurnal_cafe_transaksi']);
        }

        if (isset($params['id']) && $params['id'] != null) {
            $this->db->where('a.id', $params['id']);
        }

        $this->_query_datatable($params);

        return $this->db->get()->num_rows();
    }

    public function get_cafe_jurnal_trx()
    {
    }

    public function autoGenerate($prefix, $digit)
    {

        $nextval = 0;

        $this->db->select_max('kode_transaksi');
        $this->db->from('cafe_jurnal');
        $this->db->like('kode_transaksi', $prefix);
        $this->db->order_by('kode_transaksi', 'desc');
        $this->db->limit(1);
        $sql = $this->db->get();

        if ($sql->num_rows() > 0) {
            $nextval = str_replace($prefix . '-', '', $sql->row()->kode_transaksi);
        }

        if ($nextval == 9999) {
            $digit = 5;
        }

        $nextval += 1;

        $nextval = sprintf("%0" . $digit . "d", $nextval);
        return $prefix . '-' . $nextval;
    }

    public function add($params)
    {

        $this->db->trans_begin();
        $prefix = 'CAFE';
        $digit = 5;
        $data_jurnal_cafe = array(
            'kode_transaksi' => $this->autoGenerate($prefix, $digit),
            'jenis_transaksi' => $params['cafe_jenis_transaksi'],
            'bayar' => str_replace('.', '', $params['bayar']),
            'kembali' => str_replace('.', '', $params['kembali']),
            'total_transaksi' => str_replace('.', '', $params['total_transaksi']),
            'nama' => !empty($params['nama']) ? $params['nama'] : null,
            'email' => !empty($params['email']) ? $params['email'] : null,
            'telp' => !empty($params['telp']) ? $params['telp'] : null,
            // 'vendor' => !empty($params['vendor']) ? $params['vendor'] : null,
            'created_date' => date('Y-m-d H:m:s'),
            'created_by' => $params['created_by'],
        );

        $this->db->insert('cafe_jurnal', $data_jurnal_cafe);
        $id_cafe_jurnal = $this->db->insert_id();

        foreach ($params['cafe_jurnal'] as $key => $value) {
            $data_jurnal_cafe_trx = array(
                'id_cafe_jurnal' => $id_cafe_jurnal,
                'jenis_transaksi' => $params['jenis_jurnal_transaksi'],
                'jumlah_transaksi' => $value['jumlah_transaksi'],
                'nama_transaksi' => $value['nama_transaksi'],
                'tgl_transaksi' => $params['tgl_transaksi'],
            );

            // echo "<pre/>";print_r($data_jurnal_cafe);
            $this->db->insert('cafe_jurnal_transaksi', $data_jurnal_cafe_trx);
        }

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();

            $resp = [
                'status' => 400,
                'message' => 'Gagal Menyimpan Data',

            ];
        } else {
            $this->db->trans_commit();

            $resp = [
                'status' => 200,
                'message' => 'Berhasil Menyimpan Data',
                'data' => [
                    'id' => $id_cafe_jurnal,
                ],
            ];
        }

        return $resp;
    }

    public function update($params)
    {
        $this->db->trans_begin();

        $data_jurnal_cafe = array(
            // 'kode_transaksi'=>$params['kode_transaksi'],
            'jenis_transaksi' => null,
            'bayar' => str_replace('.', '', $params['bayar']),
            'kembali' => str_replace('.', '', $params['kembali']),
            'total_transaksi' => str_replace('.', '', $params['total_transaksi']),
            'nama' => $params['nama'],
            'email' => $params['email'],
            'telp' => $params['telp'],
            'vendor' => $params['vendor'],
            'created_date' => date('Y-m-d H:m:s'),
            'created_by' => $params['created_by'],
        );

        $this->db->where('id', $params['id_cafe_jurnal']);
        $this->db->update('cafe_jurnal', $data_jurnal_cafe);
        // $id_cafe_jurnal = $this->db->insert_id();

        $data_jurnal_cafe_trx = array(
            'jenis_transaksi' => $params['jenis_jurnal_transaksi'],
            'jumlah_transaksi' => str_replace('.', '', $params['total_transaksi']),
            // 'nama_akun'=>$params['nama_akun'],
            'nama_transaksi' => $params['nama_transaksi'],
            'tgl_transaksi' => $params['tgl_transaksi'],
        );

        $this->db->where('id', $params['id']);
        $this->db->update('cafe_jurnal_transaksi', $data_jurnal_cafe_trx);
        // return $data_jurnal_trx_cafe;

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();

            $resp = [
                'status' => 400,
                'message' => 'Gagal Mengupdate Data',
            ];
        } else {
            $this->db->trans_commit();

            $resp = [
                'status' => 200,
                'message' => 'Berhasil Mengupdate Data',
            ];
        }

        return $resp;
    }

    public function delete($params)
    {
        // echo $id;die;
        // $data = json_decode($params);
        // print_r(json_decode($params));die;
        $this->db->trans_begin();

        $this->db->where([
            'id' => $params['id'],
            'id_cafe_jurnal' => $params['id_cafe_jurnal'],
        ]);
        $this->db->delete('cafe_jurnal_transaksi');

        $this->db->where('id', $params['id_cafe_jurnal']);
        $this->db->delete('cafe_jurnal');

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();

            $resp = [
                'status' => 400,
                'message' => 'Gagal Menyimpan Data',
            ];
        } else {
            $this->db->trans_commit();

            $resp = [
                'status' => 200,
                'message' => 'Berhasil Menghapus Data',
            ];
        }

        return $resp;
    }
}
