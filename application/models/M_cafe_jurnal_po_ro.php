<?php
defined('BASEPATH') or exit('No direct script allowed');

class M_cafe_jurnal_po_ro extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->_table = 'cafe_so_log';
    }

    public function get($params)
    {
        $result = $this->find(false, $params);
        $count = ($result) ? count($this->find()) : 0;

        return [
            'result' => $result,
            // 'debug' => $this->db->last_query(),
            'total_record' => $count,
        ];
    }

    private function find($conditions = false, $params = false)
    {
        $this->db->select('
        a.id,
        d.kode_transaksi,
        d.jenis_transaksi,
        d.total_transaksi,
        d.nama,
        d.created_date,
        d.created_by,
        d.bayar,
        d.kembali
        ')
            ->from($this->_table . ' a')
            ->join('cafe_so b', 'b.id = a.id_cafe_so', 'left')
            ->join('cafe_jurnal_transaksi c', 'c.id = b.id_cafe_jurnal_transaksi', 'left')
            ->join('cafe_jurnal d', 'd.id = c.id_cafe_jurnal', 'left');

        if($params){
            $this->db->where("(d.created_date BETWEEN '" . $params['date_begin'] . " 00:00:00' and '" . $params['date_end'] . " 23:59:59')");
            $this->db->limit($params['limit'], $params['offset']);
        }

        $data = $this->db->get()->result_array();
        return $data;
    }

    public function autoGenerate($prefix, $digit)
    {

        $nextval = 0;

        $this->db->select_max('kode_transaksi');
        $this->db->from('cafe_jurnal');
        $this->db->like('kode_transaksi', $prefix);
        $this->db->order_by('kode_transaksi', 'desc');
        $this->db->limit(1);
        $sql = $this->db->get();

        if ($sql->num_rows() > 0) {
            $nextval = str_replace($prefix . '-', '', $sql->row()->kode_transaksi);
        }

        if ($nextval == 9999) {
            $digit = 5;
        }

        $nextval += 1;

        $nextval = sprintf("%0" . $digit . "d", $nextval);
        return $prefix . '-' . $nextval;
    }
}
