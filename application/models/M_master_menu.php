<?php defined('BASEPATH') or exit('No direct script access allowed');

class M_master_menu extends MY_model
{
    //SERver Side Menu
    public function get_master_menu($params)
    {
        $params['column_order']   = array('a.tipe', 'a.nama', 'a.harga', 'a.sat_kecil');
        $params['column_search']  = array('a.tipe', 'a.nama', 'a.harga', 'a.sat_kecil');
        $params['order']          = array('a.id' => 'DESC');

        $this->db->select('
            a.id id,
            a.tipe tipe,
            a.nama nama,
            a.harga harga,
            a.sat_kecil sat_kecil,
        ');
        $this->db->from('cafe_master a');
        $this->db->where('a.del_date', null);
        $this->db->where('a.tipe', 'MENU');

        if (!isset($params['offset'])) {
            $params['offset'] = '0';
        }

        if (isset($params['limit']) && $params['limit'] > 0) {
            $this->db->limit($params['limit'], $params['offset']);
        }

        $this->_query_datatable($params);

        $result = $this->db->get()->result_array();

        return [
            'result' => $result,
            'record_total' => $this->_getTotal($params),
            'record_filter' => $this->_getFiltered($params),
        ];
    }

    private function _query_datatable($params)
    {
        $i = 0;
        if (isset($params['search']) && $params['search'] != '') {
            $this->db->group_start();
            foreach ($params['column_search'] as $key => $items) {
                if ($i == 0) {
                    $this->db->like($items, $params['search']);
                } else {
                    $this->db->or_like($items, $params['search']);
                }
                $i++;
            }
            $this->db->group_end();
        }

        if (isset($params['sort_column'])) {
            if ($params['sort_column'] == 0) {
                $column_ui = 0;
            } else {
                $column_ui = $params['sort_column'];
            }
        }

        if (isset($params['sort_column'])) {
            $this->db->order_by($params['order_column'][$column_ui], $params['sort_type']);
        } else {
            $this->db->order_by(key($params['order']), $params['order'][key($params['order'])]);
        }
    }

    public function _getTotal($params)
    {

        $this->db->select('
            a.id id,
        ');
        $this->db->from('cafe_master a');
        $this->db->where('a.del_date', null);
        $this->db->where('a.tipe', 'MENU');

        return $this->db->get()->num_rows();
    }

    public function _getFiltered($params)
    {
        $params['column_order']   = array('a.tipe', 'a.nama', 'a.harga', 'a.sat_kecil');
        $params['column_search']  = array('a.tipe', 'a.nama', 'a.harga', 'a.sat_kecil');
        $params['order']          = array('a.id' => 'ASC');

        $this->db->select('
            a.id id,
        ');
        $this->db->from('cafe_master a');
        $this->db->where('a.del_date', null);
        $this->db->where('a.tipe', 'MENU');

        $this->_query_datatable($params);

        return $this->db->get()->num_rows();
    }
    // End

    // add master menu
    public function add_master_menu($params)
    {
        $this->db->insert('cafe_master', $params);
        $id = $this->db->insert_id();
        return $id;
    }

    // update master menu
    public function update_master_menu($params)
    {
        $this->db->where('id', $params['id']);
        return $this->db->update('cafe_master', $params);
    }

    //Get sat kecil
    public function get_sat_kecil($params)
    {
        $this->db->select('nama');
        $this->db->from('cafe_master');
        $this->db->where('del_date', null);
        $this->db->where('tipe', 'SAT_KECIL');
        return $this->db->get()->result_array();
    }
}
