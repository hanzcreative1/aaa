<?php defined('BASEPATH') or exit('No direct script access allowed');

class M_cafe_so extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->_table_name = 'cafe_so';
        $this->_table_alias = 'SO Master';

        $this->_table_rules = [
            [
                'field' => 'id_cafe_jurnal_transaksi',
                'label' => 'ID Jurnal Transaksi',
                'rules' => 'required',
            ],
            [
                'field' => 'nama_barang',
                'label' => 'Nama Barang',
                'rules' => 'required',
            ],
            [
                'field' => 'sisa_stock',
                'label' => 'Sisa Stok',
                'rules' => 'required',
            ],
            [
                'field' => 'updated_date',
                'label' => 'Tanggal',
                'rules' => 'required',
            ],
        ];
    }

    public function find($id = false, $conditions = false, $show_del = false, $selected_id = 0)
    {
        $this->db->select('a.id, a.sisa_stock')
            ->from($this->_table_name . ' a');

        $this->db->order_by('a.id', 'desc');

        // Jika cari berdasarkan id
        if ($id) {

            $this->db->where([
                'a.id' => $id,
            ]);

            $data = $this->db->get()->row_array();

        } else { // Jika cari semua
            if ($conditions) {
                $this->db->where($conditions);
            }

            $this->db->order_by('a.id', 'desc');

            $data = $this->db->get()->result_array();
        }

        return $data;
    }

}
