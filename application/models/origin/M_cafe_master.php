<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_cafe_master extends MY_Model 
{

    public function __construct()
    {
        parent::__construct();
        $this->_table_name = 'cafe_master';
        $this->_table_alias = 'Master referensi';

        $this->_table_rules = [
            // [
            //     'field' => '',
            //     'label' => '',
            //     'rules' => '',
            // ],
        ];
    }

    public function find($id = false, $conditions = false, $show_del = false, $selected_id = 0)
    {
        $this->db->select('a.id, a.nama, a.harga')
            ->from($this->_table_name . ' a');

        if (!$show_del) {
            $this->db->where('a.del_date', null);
        }

        $this->db->order_by('a.id', 'desc');

        // Jika cari berdasarkan id
        if ($id) {

            $this->db->where([
                'a.id' => $id,
            ]);

            $data = $this->db->get()->row_array();

        } else { // Jika cari semua
            if ($conditions) {
                $this->db->where($conditions);
            }

            $this->db->order_by('a.id', 'desc');

            $data = $this->db->get()->result_array();
        }

        return $data;
    }

}