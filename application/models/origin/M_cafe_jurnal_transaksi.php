<?php defined('BASEPATH') or exit('No direct script access allowed');

class M_cafe_jurnal_transaksi extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->_table_name = 'cafe_jurnal_transaksi';
        $this->_table_alias = 'Jurnal Transaksi';

        $this->_table_rules = [
            [
                'field' => 'id_cafe_jurnal',
                'label' => 'ID Jurnal',
                'rules' => 'required',
            ],
            [
                'field' => 'jenis_transaksi',
                'label' => 'Jenis Transaksi',
                'rules' => 'required',
            ],
            [
                'field' => 'nama_transaksi',
                'label' => 'Nama Transaksi',
                'rules' => 'required',
            ],
            [
                'field' => 'jumlah_transaksi',
                'label' => 'Jumlah',
                'rules' => 'required',
            ],
        ];
    }

    public function find($id = false, $conditions = false, $show_del = false, $selected_id = 0)
    {
        $this->db->select('a.id')
            ->from($this->_table_name . ' a');

        if (!$show_del) {
            $this->db->where('a.del_date', null);
        }

        $this->db->order_by('a.id', 'desc');

        // Jika cari berdasarkan id
        if ($id) {

            $this->db->where([
                'a.id' => $id,
            ]);

            $data = $this->db->get()->row_array();

        } else { // Jika cari semua
            if ($conditions) {
                $this->db->where($conditions);
            }

            $this->db->order_by('a.id', 'desc');

            $data = $this->db->get()->result_array();
        }

        return $data;
    }

}
