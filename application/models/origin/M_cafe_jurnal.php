<?php defined('BASEPATH') or exit('No direct script access allowed');

class M_cafe_jurnal extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->_table_name = 'cafe_jurnal';
        $this->_table_alias = 'Jurnal';

        $this->_table_rules = [
            [
                'field' => 'kode_transaksi',
                'label' => 'Kode',
                'rules' => 'required',
            ],
            [
                'field' => 'jenis_transaksi',
                'label' => 'Jenis',
                'rules' => 'required',
            ],
            [
                'field' => 'nama',
                'label' => 'Nama Vendor',
                'rules' => 'required',
            ],
            [
                'field' => 'created_date',
                'label' => 'Tanggal',
                'rules' => 'required',
            ],
            [
                'field' => 'created_by',
                'label' => 'Operator',
                'rules' => 'required',
            ],
        ];
    }

    public function find($id = false, $conditions = false, $show_del = false, $selected_id = 0)
    {
        $this->db->select('a.id')
            ->from($this->_table_name . ' a');

        if (!$show_del) {
            $this->db->where('a.del_date', null);
        }

        $this->db->order_by('a.id', 'desc');

        // Jika cari berdasarkan id
        if ($id) {

            $this->db->where([
                'a.id' => $id,
            ]);

            $data = $this->db->get()->row_array();

        } else { // Jika cari semua
            if ($conditions) {
                $this->db->where($conditions);
            }

            $this->db->order_by('a.id', 'desc');

            $data = $this->db->get()->result_array();
        }

        return $data;
    }

    public function generate_kode_transaksi($prefix = "CAFE", $digit = 5)
    {  
        $this->db->select_max('kode_transaksi');
        $this->db->from($this->_table_name);
        $this->db->like('kode_transaksi', $prefix);
        $this->db->order_by('kode_transaksi', 'desc');
        $this->db->limit(1);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $last_kode_transaksi = str_replace($prefix . '-', '', $query->row()->kode_transaksi);
        }

        $nextval = $last_kode_transaksi + 1;
        $nextval = sprintf("%0" . $digit . "d", $nextval);
        return $prefix . '-' . $nextval;
    }

}
