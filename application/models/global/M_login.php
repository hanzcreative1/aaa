<?php defined('BASEPATH') or exit('No direct script access allowed');

use \Firebase\JWT\JWT;
use \Firebase\JWT\SignatureInvalidException;
use \Firebase\JWT\ExpiredException;

class M_login extends CI_Model
{


    public function check_token($token = null)
    {
        try {
            JWT::$leeway = 6000;
            $decode = JWT::decode($token, $this->config->item('encryption_key_jwt'), array('HS256'));
            $res = array(
                'status'  => 202,
                'message' => 'Token masih berlaku',
                'data'    => (array) $decode
            );
            return $res;
        } catch (\Exception  $e) {
            $res = array('status' => 401, 'message' => 'Token tidak berlaku, Silahkan login');
            return $res;
        }
    }
}
