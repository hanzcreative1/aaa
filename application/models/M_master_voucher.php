<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_master_voucher extends MY_Model
{

    //SERver Side Voucher
    public function get_master_voucher($params)
    {
        $params['column_order']   = array('a.kode_voucher', 'a.kuota', 'a.minimal_transaksi', 'a.diskon_persen_normal');
        $params['column_search']  = array('a.kode_voucher', 'a.kuota', 'a.minimal_transaksi', 'a.diskon_persen_normal');
        $params['order']          = array('a.id' => 'DESC');

        $this->db->select('
               a.id id,
               a.kode_voucher kode_voucher,
               a.kuota kuota,
               a.minimal_transaksi minimal_transaksi,
               a.diskon_persen_normal diskon_persen_normal,
               a.tanggal_mulai tanggal_mulai,
               a.tanggal_selesai tanggal_selesai,
               a.maksimal_transaksi maksimal_transaksi,
               a.diskon_nominal_normal diskon_nominal_normal,
               a.diskon_persen_maksimal diskon_persen_maksimal,
               a.diskon_nominal_maksimal diskon_nominal_maksimal
           ');
        $this->db->from('cafe_voucher a');
        $this->db->where('a.del_date', null);
        $this->db->where('a.is_active', 'YES');

        if (!isset($params['offset'])) {
            $params['offset'] = '0';
        }

        if (isset($params['limit']) && $params['limit'] > 0) {
            $this->db->limit($params['limit'], $params['offset']);
        }

        $this->_query_datatable($params);

        $result = $this->db->get()->result_array();

        return [
            'result' => $result,
            'record_total' => $this->_getTotal($params),
            'record_filter' => $this->_getFiltered($params),
        ];
    }

    private function _query_datatable($params)
    {
        $i = 0;
        if (isset($params['search']) && $params['search'] != '') {
            $this->db->group_start();
            foreach ($params['column_search'] as $key => $items) {
                if ($i == 0) {
                    $this->db->like($items, $params['search']);
                } else {
                    $this->db->or_like($items, $params['search']);
                }
                $i++;
            }
            $this->db->group_end();
        }

        if (isset($params['sort_column'])) {
            if ($params['sort_column'] == 0) {
                $column_ui = 0;
            } else {
                $column_ui = $params['sort_column'];
            }
        }

        if (isset($params['sort_column'])) {
            $this->db->order_by($params['order_column'][$column_ui], $params['sort_type']);
        } else {
            $this->db->order_by(key($params['order']), $params['order'][key($params['order'])]);
        }
    }

    public function _getTotal($params)
    {

        $this->db->select('a.id id');
        $this->db->from('cafe_voucher a');
        $this->db->where('a.del_date', NULL);
        $this->db->where('a.is_active', 'YES');

        return $this->db->get()->num_rows();
    }

    public function _getFiltered($params)
    {
        $params['column_order']   = array('a.kode_voucher', 'a.kuota', 'a.minimal_transaksi', 'a.diskon_persen_normal');
        $params['column_search']  = array('a.kode_voucher', 'a.kuota', 'a.minimal_transaksi', 'a.diskon_persen_normal');
        $params['order']          = array('a.id' => 'ASC');

        $this->db->select('
               a.id id,
           ');
        $this->db->from('cafe_voucher a');
        $this->db->where('a.del_date', NULL);
        $this->db->where('a.is_active', 'YES');

        $this->_query_datatable($params);

        return $this->db->get()->num_rows();
    }
    // End

    // add master voucher
    public function add_master_voucher($params)
    {
        $this->db->insert('cafe_voucher', $params);
        $id = $this->db->insert_id();
        return $id;
    }

    // update master voucher
    public function update_master_voucher($params)
    {
        $this->db->where('id', $params['id']);
        return $this->db->update('cafe_voucher', $params);
    }
}
