<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_cafe_master extends CI_Model{
    
    public function __construct(){
        parent::__construct();
        $this->_table="cafe_master";
    }

    private function _get_datatables($params){
        // print_r($params);
        $i=0;
        if(isset($params['search']) && $params['search']!=null){
            $this->db->group_start();
            foreach($params['column_search'] as $key => $items){
                if($i==0){
                    $this->db->like($items,$params['search']);
                }else{
                    $this->db->or_like($items,$params['search']);

                }
                $i++;
            }
            $this->db->group_end();
        }

        if(isset($params['sort_column'])){
            if($params['sort_column']==0){
                $column_ui=0;
            }else{
                $column_ui=$params['sort_column']-1;
            }
        }
        
        if(isset($params['sort_column'])){
            $this->db->order_by($params['column_order'][$column_ui],$params['sort_type']);
        }elseif(isset($params['order'])){
            $this->db->order_by(key($params['order']),$params['order'][key($params['order'])]);

        }
        // echo $this->db->last_query();
    }

    public function get($params){
        
        $params['column_order'] = ['',"tipe","nama","harga","qty_sat_kecil","sat_kecil","alamat",'telp',"email","logo"];
        $params['column_search'] = ["tipe","nama","harga","qty_sat_kecil","sat_kecil","alamat",'telp',"email","logo"];
        $params['order'] = ['id'=>'asc'];
        
        $this->db->select('
            id,
            tipe,
            nama,
            harga,
            qty_sat_kecil,
            sat_kecil,
            alamat,telp,
            email,
            img logo
        ');
        $this->db->from($this->_table);

        $this->db->where('del_date',null);

        if(isset($params['tipe']) && $params['tipe']!=null){
            $this->db->where('tipe',$params['tipe']);
        }

        if(isset($params['id']) && $params['id']!=''){
            $this->db->where('id',$params['id']);
        }

        if(!isset($params['offset']) && $params['offset']==''){
            $params['offset']=0;
        }

        if(isset($params['limit']) && $params['offset'] >0){
            $this->db->limit($params['limit'],$params['offset']);
        }

        $this->_get_datatables($params);

        $sql = $this->db->get();
        $result=$sql->result_array();
        
        // echo $this->db->last_query();
        
        return [
            'total_record'=>$this->total_record($params),
            'filtered'=>$this->filter($params),
            'result'=>$result
        ];

    }

    public function filter($params){

        $params['column_order'] = ['',"tipe","nama","harga","qty_sat_kecil","sat_kecil","alamat",'telp',"email","logo"];
        $params['column_search'] = ["tipe","nama","harga","qty_sat_kecil","sat_kecil","alamat",'telp',"email","logo"];
        $params['order'] = ['id'=>'asc'];

        $this->db->select('id,tipe,nama,harga,qty_sat_kecil,sat_kecil,alamat,telp,email,logo');
        $this->db->from($this->_table);

        $this->db->where('del_date',null);

        if(isset($params['tipe']) && $params['tipe']!=null){
            $this->db->where('tipe',$params['tipe']);
        }    

        if(isset($params['id']) && $params['id']!=''){
            $this->db->where('id',$params['id']);
        }

        if(!isset($params['offset']) && $params['offset']==''){
            $params['offset']=0;
        }

        if(isset($params['limit']) && $params['offset'] >0){
            $this->db->limit($params['limit'],$params['offset']);
        }

        $this->_get_datatables($params);

        return $this->db->count_all_results();

    }

    public function total_record($params){

        $this->db->select('id,tipe,nama,harga,qty_sat_kecil,sat_kecil,alamat,telp,email,logo');
        $this->db->from($this->_table);

        $this->db->where('del_date',null);

        if(isset($params['tipe']) && $params['tipe']!=null){
            $this->db->where('tipe',$params['tipe']);
        }    

        if(isset($params['id']) && $params['id']!=''){
            $this->db->where('id',$params['id']);
        }

        return $this->db->count_all_results();
    }

    public function add($params){

        foreach ($params as $k => $v) {
            if (empty($v)) {
                unset($params[$k]);
            }
        }
        
        $this->db->trans_begin();

        $this->db->insert($this->_table,$params);

        if($this->db->trans_status()===FALSE){
            $this->db->trans_rollback();
            
            $resp =[
                'status'=>400,
                'message'=>'Gagal menyimpan data'
            ];

        }else{
            $this->db->trans_commit();
            $resp =[
                'status'=>200,
                'message'=>'Berhasil menyimpan data'
            ];
        }

        return $resp;
    }

    public function update($params){
        // print_r($params);
        $this->db->trans_begin();

        $this->db->where('id',$params['id']);
        $this->db->update($this->_table,$params);

        if($this->db->trans_status()===FALSE){
            $this->db->trans_rollback();
            
            $resp =[
                'status'=>400,
                'message'=>'Gagal menyimpan data'
            ];

        }else{
            $this->db->trans_commit();
            $resp =[
                'status'=>200,
                'message'=>'Berhasil menyimpan data'
            ];
        }
        // echo $this->db->last_query();
        return $resp;

    }

    public function delete($params){

        $this->db->trans_begin();

        $this->db->where('id',$params['id']);
        $this->db->update($this->_table,[
            'del_date'=>date('Y-m-d H:i:s')
        ]);

        if($this->db->trans_status()===FALSE){
            $this->db->trans_rollback();
            
            $resp =[
                'status'=>400,
                'message'=>'Gagal menghapus data'
            ];

        }else{
            $this->db->trans_commit();
            $resp =[
                'status'=>200,
                'message'=>'Berhasil menghapus data'
            ];
        }
      
        return $resp;
    }
}