<?php
defined('BASEPATH') or exit('No direct script access allowed');

$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['master_menu'] = 'C_master_menu';
$route['master_menu/(:any)'] = 'C_master_menu/$1';

$route['master_voucher'] = 'C_master_voucher';
$route['master_voucher/(:any)'] = 'C_master_voucher/$1';

$route['cafe_jurnal_transaksi'] = 'C_cafe_jurnal_transaksi';
$route['cafe_jurnal_transaksi/(:any)'] = 'C_cafe_jurnal_transaksi/$1';
$route['cafe_master/(:any)'] = 'C_cafe_master/$1';
$route['cafe_jurnal_po_ro)'] = 'C_cafe_jurnal_po_ro';
$route['cafe_jurnal_po_ro/(:any)'] = 'C_cafe_jurnal_po_ro/$1';
