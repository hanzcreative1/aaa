<?php
defined('BASEPATH') or exit('No direct script access allowed');

class C_cafe_jurnal_po_ro extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_cafe_jurnal_po_ro', 'model');
        $this->load->model('origin/M_cafe_master', 'master');
        $this->load->model('origin/M_cafe_jurnal', 'jurnal');
        $this->load->model('origin/M_cafe_jurnal_transaksi', 'jurnal_transaksi');
        $this->load->model('origin/M_cafe_so', 'so');
        $this->load->model('origin/M_cafe_so_log', 'so_log');
    }

    public function index_get()
    {
        $params = $this->get();
        $data = $this->model->get($params);

        if (!$data) {
            $resp = [
                'status' => 400,
                'message' => 'Data tidak ditemukan',
            ];
        } else {
            $resp = [
                'status' => 200,
                'data' => $data,
                'message' => 'Data ditemukan',
            ];
        }

        $this->response($resp, $resp['status']);
    }

    /**
     * POST
     * Beroperasi data ke tabel :
     * 1. insert cafe_jurnal
     * 2. insert cafe_jurnal_transaksi
     * 3. insert atau update cafe_so
     * 3. insert cafe_so_log
     */
    public function index_post()
    {
        $input = $this->post();

        if (empty($input)) {
            $this->response('Data tidak tersedia', '201');
            die();
        }

        $this->db->trans_begin();

        //simpan ke table jurnal
        $data_jurnal = $input['data'];
        $date = $data_jurnal['date'];
        $data = [
            'kode_transaksi' => $this->jurnal->generate_kode_transaksi(),
            'jenis_transaksi' => 'PO/RO',
            'bayar' => $data_jurnal['pay'],
            'kembali' => $data_jurnal['payBack'],
            'total_transaksi' => $data_jurnal['totalPrice'],
            'nama' => $data_jurnal['vendor'],
            'email' => $data_jurnal['email'],
            'telp' => $data_jurnal['wa'],
            'created_date' => $date,
            'created_by' => $data_jurnal['operator'],
        ];

        // Simpan ke tabel jurnal
        $save = $this->jurnal->save($data);
        $this->query_check($save);

        // Simpan ke tabel jurnal_transaksi
        $data_jurnal_transaksi = $input['data']['details'];
        $id_jurnal = $this->db->insert_id();

        foreach ($data_jurnal_transaksi as $k => $v) {
            $detail = [
                'id_cafe_jurnal' => $id_jurnal,
                'tgl_transaksi' => $date,
                'nama_transaksi' => $v[0],
                'jenis_transaksi' => 'KREDIT',
                'jumlah_transaksi' => $v[1],
                'qty' => $v[2],
            ];

            $save = $this->jurnal_transaksi->save($detail);
            $id_jurnal_transaksi = $this->db->insert_id();
            $this->query_check($save);

            // Cek apakah barang sudah terdaftar di table so
            $cek = $this->so->find(false, ['nama_barang' => $v[0]]);
            if ($cek) {

                // Update ke table so
                $save = $this->so->save([
                    'id' => $cek[0]['id'],
                    'sisa_stock' => $cek[0]['sisa_stock'] + $v[2],
                ], true); // Update dengan mengabaikan rules validasi
                $this->query_check($save);

                $id_cafe_so = $cek[0]['id'];
            } else {
                // Simpan ke table so
                $save = $this->so->save([
                    'id_cafe_jurnal_transaksi' => $id_jurnal_transaksi,
                    'nama_barang' => $v[0],
                    'sisa_stock' => $v[2],
                    'updated_date' => $date,
                ]);
                $this->query_check($save);

                //simpan ke table so_log
                $id_cafe_so = $this->db->insert_id();
                $this->query_check($save);
            }

            $save = $this->so_log->save([
                'id_cafe_so' => $id_cafe_so,
                'jenis' => "MASUK",
                'qty' => $v[2],
                'created_date' => $date,
            ]);
            $this->query_check($save);

        }

        $this->db->trans_commit();
        $this->response($save, $save['status']);
    }

    // public function index_put()
    // {
    //     if ($this->put() == '') {
    //         $resp = [
    //             'status' => 400,
    //             'message' => 'Data tidak boleh kosong',
    //         ];

    //         $this->response($resp, $resp['status']);
    //     }

    //     $udpate = $this->model->update($this->put());
    //     $this->response($udpate, $udpate['status']);
    // }

    // public function index_delete()
    // {
    //     if ($this->delete() == '') {
    //         $resp = [
    //             'status' => 400,
    //             'message' => 'Data tidak boleh kosong',
    //         ];

    //         $this->response($resp, $resp['status']);
    //     }

    //     $delete = $this->model->delete($this->delete());
    //     $this->response($delete, $delete['status']);
    // }

    // public function noTrx_get()
    // {

    //     $noTrx = $this->model->autoGenerate('CAFE', 5);

    //     $this->response(array('status' => 200, $noTrx), 200);
    // }

    public function add_variables_get()
    {
        $data = [
            'vendor' => $this->master->find(false, [
                'a.tipe' => 'VENDOR',
            ]),
            'product' => $this->master->find(false, [
                'a.tipe' => 'PO/RO',
            ]),
            'operator' => $this->master->find(false, [
                'a.tipe' => 'OPERATOR',
            ]),
        ];

        if (!$data) {
            $resp = [
                'status' => 400,
                'message' => 'Data tidak ditemukan',
            ];
        } else {
            $resp = [
                'status' => 200,
                'data' => $data,
                'message' => 'Data ditemukan',
            ];
        }

        $this->response($resp, $resp['status']);
    }
}
