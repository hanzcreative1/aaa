<?php
    defined('BASEPATH') or exit('No direct script access allowed');

    class C_cafe_master extends MY_Controller{

        public function __construct(){
            parent::__construct();
            $this->load->model('M_cafe_master','model');
        }
        
        public function index_get(){

            $data = $this->model->get($this->get());

            if($data==null){
                $resp=[
                    'status'=>400,
                    'message'=>'Data tidak ditemukan'
                ];
            }else{
                $resp=[
                    'status'=>200,
                    'message'=>'Success',
                    'data'=>$data
                ];  
            }

            $this->response($resp,$resp['status']);
        }

        public function index_post(){

            if($this->post()==null){
                $resp= [
                    'status'=>400,
                    'message'=>'Data tidak boleh kosong'
                ];
                
                $this->response($resp,$resp['status']);
            }

            // print_r($this->post());
            $save =$this->model->add($this->post());

            $this->response($save,$save['status']);
        }

        public function index_put(){

            if($this->put()==null){
                $resp= [
                    'status'=>400,
                    'message'=>'Data tidak boleh kosong'
                ];
                
                $this->response($resp,$resp['status']);
            }

            $save =$this->model->update($this->put());
            $this->response($save,$save['status']);
        }

        public function index_delete(){


            if($this->delete()==null){
                $resp= [
                    'status'=>400,
                    'message'=>'Data tidak boleh kosong'
                ];
                
                $this->response($resp,$resp['status']);
            }

            $del =$this->model->delete($this->delete());
            $this->response($del,$del['status']);
        }
    }