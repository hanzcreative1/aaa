<?php
defined('BASEPATH')
    or exit('No direct script access allowed');

class C_cafe_jurnal_transaksi extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_cafe_jurnal_transaksi', 'model');
    }

    public function index_get()
    {
        $params = $this->get();
        $data   = $this->model->get($params);

        if (!$data) {
            $res['status']  = '200';
            $res['message'] = 'Gagal mendapatkan data cafe jurnal transaksi';
            $res['data']    = [];
        } else {
            $res['status']  = '200';
            $res['message'] = 'Berhasil mendapatkan data cafe jurnal transaksi';
            $res['data']    = $data;
        }

        $this->response($res, $res['status']);
    }

    public function index_post()
    {
        if ($this->post() == '') {
            $resp = [
                'status' => 400,
                'message' => 'Data tidak boleh kosong'
            ];

            $this->response($resp, $resp['status']);
        }

        $add = $this->model->add($this->post());
        $this->response($add, $add['status']);
    }

    public function index_put()
    {
        if ($this->put() == '') {
            $resp = [
                'status' => 400,
                'message' => 'Data tidak boleh kosong'
            ];

            $this->response($resp, $resp['status']);
        }

        $udpate = $this->model->update($this->put());
        $this->response($udpate, $udpate['status']);
    }

    public function index_delete()
    {
        // print_r($this->delete());die;
        if ($this->delete() == '') {
            $resp = [
                'status' => 400,
                'message' => 'Data tidak boleh kosong'
            ];

            $this->response($resp, $resp['status']);
        }

        $delete = $this->model->delete($this->delete());
        $this->response($delete, $delete['status']);
    }

    public function noTrx_get()
    {

        $noTrx = $this->model->autoGenerate('CAFE', 5);

        $this->response(array('status' => 200, $noTrx), 200);
    }
}
