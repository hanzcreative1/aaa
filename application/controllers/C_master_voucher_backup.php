<?php
defined('BASEPATH')
    or exit('No direct script access allowed');

class C_master_voucher extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        //        $this->load->model('M_master_voucher', 'model');
    }
    public function index_get()
    {
        $params = $this->get();
        //      $data = $this->model->get_master_voucher($params);

        $res['status']  = '200';
        $res['message'] = 'Berhasil mendapatkan data master voucher';
        //    $res['data']    = $data;

        $this->response($res, $res['status']);
    }
    /*
    public function index_post()
    {
        $params = $this->post();
        $data = $this->model->add_master_voucher($params);

        $res['status']  = '200';
        $res['message'] = 'Berhasil menambahkan data master voucher';

        $this->response($res, $res['status']);
    }

    public function index_put()
    {
        $params = $this->put();
        $data = $this->model->update_master_voucher($params);

        $res['status']  = '200';
        $res['message'] = 'Berhasil mengubah data master voucher';

        $this->response($res, $res['status']);
    }

    public function index_delete()
    {
        $params = $this->delete();
        $data = $this->model->update_master_voucher($params);

        $res['status']  = '200';
        $res['message'] = 'Berhasil mengubah data master voucher';

        $this->response($res, $res['status']);
    }
*/
}
